"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    starttime = request.args.get("starttime", 999,type=str)
    startdate = request.args.get("startdate",999,type=str)
    brevit_dist = request.args.get("brevdist",999,type=int)


    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))


    # need to make the starttime and date into arrow object 
    start = arrow.get(startdate + " " + starttime)
    

    app.logger.debug(f"Merged date is {start}")
    open_time = acp_times.open_time(km, brevit_dist, start)
    app.logger.info(f"Open Time: {open_time}")
    close_time = acp_times.close_time(km, brevit_dist, start)
    app.logger.info(f"Close Time: {close_time}")
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.INFO)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
