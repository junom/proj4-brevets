import math
import arrow
import logging
import flask

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    app = flask.Flask(__name__)
    app.logger.setLevel(logging.DEBUG)
    
    
    opentime = 0
    location_list = [200,400,600,1000,1300]
    min_speeds = [15,15,15,11.428,13.333]
    init_index = 0
    distsance_left = control_dist_km

    i = 0
    while location_list[i] < control_dist_km:
        opentime += location_list[i] / min_speeds[i]
        if i == 0:
            distsance_left -= 200
        else:
            distsance_left -= location_list[i] - location_list[i-1]
    # once we get here, we are left with the remainder
    # ex: for a dist of 650, we are left with 50
    app.logger.(f"Distance Left: {distsance_left}, at bracket {location_list[i]}")




def main():

    open_time(650,200,arrow.now().isoformat)



if __name__ == "__main__":
    main()