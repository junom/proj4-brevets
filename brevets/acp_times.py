"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math
import flask
import logging 
import config
from typing import List



#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
      brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600,
         or 1000 (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control open time.
      This will be in the same time zone as the brevet start time.
   """
   app = flask.Flask(__name__)
   app.debug = True 
   app.logger.setLevel(logging.INFO)
   
   if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

   max_speeds = [34,32,30,28,26]
   time_hrs, time_mins = calc_times(max_speeds, control_dist_km)
   # time_hrs += 5
   # done when we reach 0
   app.logger.info(f"OPENING TIME {time_hrs} HOURS, {time_mins} MINS")
   return brevet_start_time.shift(hours= time_hrs, minutes= time_mins).isoformat()
         


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
         brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600, or 1000
         (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control close time.
      This will be in the same time zone as the brevet start time.
   """

   app = flask.Flask(__name__)
   app.debug = True 
   app.logger.setLevel(logging.INFO)

   # "starting" control point should close an hour after the opening time
   if (control_dist_km == 0):
      return brevet_start_time.shift(hours= 1).isoformat()

   if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

   min_speeds = [15,15,15,11.428, 13.333]
   time_hrs, time_mins = calc_times(min_speeds, control_dist_km)
   app.logger.info(f"Closing TIME {time_hrs} HOURS, {time_mins} MINS")
   return brevet_start_time.shift(hours= time_hrs, minutes= time_mins).isoformat()



def calc_times(speedlist: List, control_dist_km) -> int:
   """
   aux function to calc the times given either the min or max speed list 
   """

   app = flask.Flask(__name__)
   app.debug = True 
   app.logger.setLevel(logging.DEBUG)

   totaltime = 0
   location_list = [200,400,600,1000,1300]
   distance_left = control_dist_km
   i = 0
   while location_list[i] < control_dist_km:
      if i == 0:
         app.logger.debug(f"calcing remainder {location_list[i]} / {speedlist[i]} = {location_list[i] / speedlist[i]}")
         totaltime += location_list[i] / speedlist[i]
         distance_left -= 200
      else:
         app.logger.debug(f"calcing remainder { (location_list[i] - location_list[i-1])} / {speedlist[i]} = { (location_list[i] - location_list[i-1]) / speedlist[i]}")
         totaltime += (location_list[i] - location_list[i-1]) / speedlist[i]
         distance_left -= location_list[i] - location_list[i-1]
      i += 1
   # once we get here, we are left with the remainder
   # ex: for a dist of 650, we are left with 50
   # app.logger.info(f"Distance Left: {distsance_left}, at bracket below {location_list[i]}")
   if distance_left > 0:
      print(f"distance left after {i} iterations is {distance_left}")
      print(f"calcing remainder {distance_left} / {speedlist[i]} = {distance_left / speedlist[i]}")
      totaltime += distance_left / speedlist[i]

   time_hrs = math.floor(totaltime)
   time_mins = round((totaltime - time_hrs) * 60)

   return time_hrs, time_mins