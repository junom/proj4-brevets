# Brevet time calculator with Ajax

An implementation of the RUSA ACP controle time calculator with flask and ajax.
Credits to Michal Young for the initial version of this code.

To use, execute the run.sh file and navigate to localhost:5000 in your browser. 
Enter a control point in miles or kilometers to recieve the calculated open and close times relative to the set start date / time. Brevet distance can be selected with the dropdown menu. (Limited to ACP standard brevets)


The original calculator can be found here: 
https://rusa.org/octime_acp.html

## Background on ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). 


## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page.

This implementation fills in times as the input fields are filled using Ajax and Flask. A conversion between miles and kilometers before calculation is also implimented. 

## Assumptions and Ambiguities 

There are many ambiguities within the rules that needed to be taken into accound when constructing a controle time calculator. To handle these cases I looked to the original calculator. Such behaviors include: 

* Controle point > Brevet distance:
	* The original calculator throws an error when the user inputs a controle distance that is greater that 1.2x the brevet length. I mirror this behavior by displaying a browser dialogue box notifying the user of a the upper bound on a controle point if they enter a value that is too large. Controle points greater than the brevet distance but less than 1.2x the brevet distance are calculated as the brevet distance. ex: 205 -> 200 before open/close time calculation for a 200km brevet

* Handling invalid user input:
	* This was accomplished on the frontend by Javascript conditionals. If a user enters an alpha value instead of a number, similar notification is shown, the values are not sent to the backend and open / close times are not calculated. (Do not pass go, do not collect $200)

* 0 Distance control point
	* According to the "Oddites" Seciton of (https://rusa.org/pages/acp-brevet-control-times-calculator), a 0 distance control time should close an hour after the start time. This is handled as expected. 

## Nosetests
This repo includes the test suite I used to develop the controle point calculator domain logic. It automatically runs when the docker container is built with the provided run.sh file. 
